import sys, os
import numpy as np
import networkx as nx
import itertools
from scipy.special import binom
from scipy.linalg import solve_sylvester, logm
from matplotlib import pyplot as plt
import time


    
################################################################################
#### SOME HELPER FUNCTIONS FOR HANDLING GRAPHS AND FILES #######################
################################################################################

def filename_for_graphs(s, num_nodes, num_edges, directed, selfedges, 
                        extension=''):
    '''Returns a filename for a given set of graph attributes. 
    
    Parameters
    ----------
    s : str
       Beginning of the file name.
    
    num_nodes : int
       Number of nodes in graph(s).

    num_edges : int
       Number of edges in graph(s).

    directed : bool
        If true, file name includes the string 'directed'

    selfedges : bool
        If true, file name includes the string 'selfedges'
    
    extension : string (optional)
        End of file name. (Should include a '.')
              
    Returns
    -------
    str fname
        a file name
    '''
    
    fname = s + str(num_nodes)+'_'+str(num_edges)
    if directed: fname += '_directed'
    if selfedges: fname += '_selfedges'
    fname = fname + extension
    
    return fname


def read_pickle(fname):
    '''Read a pickle file. Yes I am lazy.
    
    Parameters
    ----------
    fname : str
       location and name of pickle file to be read
       
    Returns 
    -------
    object obj
       Whatever we found in the pickle
    '''
    
    try:
        import pickle
    except ImportError:
        raise ImportError("pickle required for read_pickle()")

    outfile = open(fname,'rb')
    obj = pickle.load(outfile)
    outfile.close()
    
    return obj


def write_pickle(fname, obj):
    '''Write or overwrite a pickle file. Yes I am lazy.
    
    Parameters
    ----------
    fname : str
       location and name of pickle file to be created or overwritten
       
    obj : any object that can be stored in a pickle
    
    Returns 
    -------
    str fname
       The location and name of the created pickle
    '''

    try:
        import pickle
    except ImportError:
        raise ImportError("pickle required for write_pickle()")

    outfile = open(fname,'wb')
    pickle.dump(obj,outfile)
    outfile.close()
    
    return fname


def hard_copy_networkx_graph(G, node_attributes=False):
    '''Creates a hard copy of a network graph G. The graph G can be a `nx.Graph` 
    or a `nx.DiGraph` object. 

    Parameters
    ----------
    G : graph
       A networkx graph
       
    node_attributes : bool (optional)
       If True, copy node attributes. Default is False.
       
    Returns
    -------
    nx.Graph or nx.DiGraph H
        a hard-copy of the graph G

    Examples
    --------
    >>> g0 = nx.dodecahedral_graph()
    >>> g0_soft_copy = g0
    >>> # if we know that g0 is nx.Graph or nx.DiGraph
    >>> g0_hard_copy = nx.Graph(g0) #or nx.DiGraph(g0)
    >>> # otherwise
    >>> g0_hard_copy2 = hard_copy_networkx_graph(g0)
    >>> for i, g in enumerate([g0, g0_soft_copy, g0_hard_copy, g0_hard_copy2]):
    >>>    print(i, g.number_of_edges())
    >>> g0.remove_edge(0,1)
    >>> for i, g in enumerate([g0, g0_soft_copy, g0_hard_copy, g0_hard_copy2]):
    >>>    print(i, g.number_of_edges())
    '''
    
    H = G.__class__()
    H.add_nodes_from(G)
    H.add_edges_from(G.edges)
    if node_attributes:
        nx.set_node_attributes(H, G.nodes())
        
    return H


################################################################################
#### FUNCTIONS FOR FINDING NON-ISOMORPHIC GRAPHS ###############################
################################################################################

def nonisomorphic_graphs(n, m, directed=True, selfedges=True,
                         isolates=False, save=True, load=True, 
                         filename=None, directory=''): 
    '''Find the set of all non-isomorphic graphs with n nodes and m edges.
    
    Perform an exhaustive search for graphs that are non-isomorphic to each 
    other. Returns the largest possible list of non-isomorphic graphs (nx.Graph 
    objects of nx.DiGraph objects) with specified numbers of nodes and edges.
    
    Parameters
    ----------
    n : int
       Number of nodes

    m : int
       Number of edges
    
    directed : bool (optional)
       If true, consider graphs to be directed and return a list of nx.DiGraph
       objects
    
    selfedges : bool (optional)
       If true, allow graphs to have self-edges
       
    save : bool (optional)
       If true, save list of nonisomorphic graphs to file
    
    load : bool (optional)
       If true, try loading list of nonisomorphic graphs from file instead of 
       computing the list from scratch
       
    filename : str (optional)
       Filename to be used for save and load options
       
    directory : str (optional)
       Path to where data is saved.
        
    Returns 
    -------
    list graphs
       A list of non-isomorphic graphs with n nodes and m edges
       
    Example
    -------
    >>> list_of_graphs = nonisomorphic_graphs(3,3, directed=False) 
    >>> plt.figure(figsize=(6,4))
    >>> for i,g in enumerate(list_of_graphs):
    >>>     ax = plt.subplot(2,3,i+1)
    >>>     ax.set_xlim([-1.25,1.75])
    >>>     ax.set_ylim([-1.5,1.5])
    >>>     shell = nx.layout.shell_layout(g)
    >>>     nx.draw(g, shell)
    >>>     draw_networkx_selfedges(g, shell)
    >>>     ax.axis('on')
    '''
    
    try:
        import os
    except ImportError:
        raise ImportError("os required for nonisomorphic_graphs()")

    try:
        import itertools
    except ImportError:
        raise ImportError("itertools required for nonisomorphic_graphs()")
        
    try:
        import numpy as np
    except ImportError:
        raise ImportError("numpy required for nonisomorphic_graphs()")

    if filename is None:
        #generate file name
        filename = filename_for_graphs('nonisomorphic_graphs', n, m, directed, 
                                       selfedges) + '.p'
    
    if load and os.path.exists(directory+filename):
        # return data that has been computed previously
        graphs = read_pickle(directory+filename)
        return graphs
    
    # set graph type and free slots for edges
    if directed:
        cu = nx.DiGraph()
        if selfedges:
            free_slots = n**2
            row_indices = np.array([i//n for i in range(free_slots)])
            col_indices = np.array([i % n for i in range(free_slots)])
            slots_list = zip(row_indices, col_indices)
        else:

            free_slots = n*(n-1)
            row_indices =  np.array([i//(n-1) for i in range(free_slots)])
            col_indices =  np.array([i % (n-1) + 1*(i % (n-1) >= i//(n-1)) 
                                     for i in range(free_slots)])
            slots_list = zip(row_indices, col_indices)
    else:
        cu = nx.Graph()
        if selfedges:
            #free_slots = n*(n+1)//2
            row_indices, col_indices = np.triu_indices(n,0)
            slots_list = zip(row_indices, col_indices)
        else:
            #free_slots = n*(n-1)//2
            row_indices, col_indices = np.triu_indices(n,1)
            slots_list = zip(row_indices, col_indices)

    # initialize list of non-isomorphic graphs
    graphs = []
 
    # go through all possible combinations of m edges in specified graph type
    for c in itertools.combinations(slots_list, m):
        
        # create new graph with this combination of edges
        g = nx.empty_graph(n, create_using=cu)    
        g.add_edges_from(c)
        
        if not isolates:
            # remove graphs with isolated nodes
            if min([g.degree(x) for x in g.nodes()])==0:
                continue
        
        #check if new graph  g is isomorphic to any previously stored ones
        isotest = lambda x: nx.is_isomorphic(g,x) 
        if not np.any(list(map(isotest, graphs))): 
            # if not, add a hard copy of g to list of non-isomorphic graphs
            graphs = graphs + [hard_copy_networkx_graph(g)]

    if save:
        # if save==True, save data for later
        write_pickle(directory+filename, graphs) 
            
    return graphs


def nonisomorphic_2FN_graphs(n, m, directed=True, selfedges=True, 
                             isolates=False, save=True, load=True, 
                             filename=None, directory=''):
    '''Find the set of all non-isomorphic graphs with n nodes (of which two
    nodes are focal nodes) and m edges.

    Perform an exhaustive search for graphs with 2 focal nodes that are non-
    isomorphic to each other. Returns the largest possible list of non-
    isomorphic graphs (nx.Graph objects oe nx.DiGraph objects) with two focal
    nodes and the specified numbers of nodes and edges.
    
    Parameters
    ----------
    n : int
       Number of nodes

    m : int
       Number of edges
    
    directed : bool (optional)
       If true, consider graphs to be directed and return a list of nx.DiGraph
       objects
    
    selfedges : bool (optional)
       If true, allow graphs to have self-edges
       
    save : bool (optional)
       If true, save list of nonisomorphic graphs to file
    
    load : bool (optional)
       If true, try loading list of nonisomorphic graphs from file instead of 
       computing the list from scratch
       
    filename : str (optional)
       Filename to be used for save and load options
       
    directory : str (optional)
       Path to where data is saved.
    
    Returns 
    -------
    list graphs
       A list of non-isomorphic graphs with n nodes and m edges and two focal 
       nodes
    '''    
    try:
        from networkx.algorithms.isomorphism import categorical_node_match
    except ImportError:
        raise ImportError("networkx required for nonisomorphic_2FN_graphs()")
    
    try:
        import os
    except ImportError:
        raise ImportError("os required for nonisomorphic_2FN_graphs()")

    try:
        import itertools
    except ImportError:
        raise ImportError("itertools required for nonisomorphic_2FN_graphs()")
        
    try:
        import numpy as np
    except ImportError:
        raise ImportError("numpy required for nonisomorphic_2FN_graphs()")

    if filename is None:
        #generate file name
        filename = filename_for_graphs('nonisomorphic_2FN_graphs',
                                       n, m, directed, selfedges) + '.p'
    
    if load:
        for d in directory:
            if os.path.exists(d+filename):
                # return data that has been computed previously
                graphs = read_pickle(d+filename)
                return graphs

    graphs = []
    nm = categorical_node_match('label', '')
    
    # loop over all non-isomorphic graphs with n nodes and m edges
    for g in nonisomorphic_graphs(n, m, directed=directed, 
                                  selfedges=selfedges, load=True,
                                  isolates=isolates, 
                                  directory=directory):
        
        # currently we haven't labeled a source or target in the graph
        # make a list for all possible structural motifs that we can get
        # from choosing a source node and target node in g
        temp_graphs = []
        
        # loop over all combinations of source node and target node
        for (i,j) in itertools.combinations(g.nodes(),2):
            
            # make a copy of g
            h = hard_copy_networkx_graph(g)
            
            # create node attribute 'label'
            nx.set_node_attributes(h, "", 'label')
            
            # label source node and target node
            h.nodes[i]['label']='i'
            h.nodes[j]['label']='j'
            
            #check if new graph  h is isomorphic (with node labels!) to any 
            # previously stored graphs
            isotest = lambda x: nx.is_isomorphic(h,x, node_match=nm) 
            if not np.any(list(map(isotest, temp_graphs))): 
                # if not, add a hard copy of h to list of non-isomorphic graphs
                temp_graphs = temp_graphs + [hard_copy_networkx_graph(h, node_attributes=True)]
                
        graphs = graphs + temp_graphs
        
    if save:
        # if save==True, save data for later
        write_pickle(directory+filename, graphs) 
                
    return graphs


################################################################################
#### FUNCTIONS FOR COMPUTING HEURISTICS FROM MATCHING MOTIFS ###################
################################################################################

def all_trails_to(g, target):
    '''Find all trails to a node in a graph g.'''
    
    if g.number_of_edges==0:
        return []
    
    else:
        if isinstance(g, nx.DiGraph):
            edge_set = g.in_edges(target)
        else:
            # g is an undirected graph
            edge_set = g.edges(target)
            
        trails = []            
        for e in edge_set: 
            h = g.copy()
            h.remove_edge(e[0],e[1])
            subtrails = all_trails_to(h, e[0]) 
            trails_via_e = [[e]]+[t+[e] for t in subtrails]
            trails = trails + trails_via_e
        
        return trails


def all_paths_to(G, target):
    '''Find all simple paths to a node in a graph G.'''
    
    nodes_with_selfedge = [i for i in G.nodes() if (i,i) in G.edges()]
    paths_to_target = []
    
    for i in G.nodes():
        paths_from_i = list(nx.all_simple_paths(G, source=i, target=target))
        paths_to_target = paths_to_target + paths_from_i
        for path in paths_from_i:
            #print(path)
        
            # check for selfedges
          
            # make a copy of list of selfedges
            nws = [p for p in nodes_with_selfedge]
            # make list of new paths. add old path first (remove later)
            new_paths = [path]
        
            for np in new_paths:
                for ni, n in enumerate(path):
                    # check if a node in the path has a selfedge
                    if n in nws:
                        # if so, make a new path that uses the selfedge when 
                        # it meets node n
                        new_path = path[:ni]+[n,n]
                        # check if n is not at the end of the path
                        if ni<len(path)-1:
                            # if so, include the rest of the path
                            new_path = new_path + path[ni+1:]
                        # add new path to list of new paths
                        new_paths = new_paths + [new_path]
                        # remove n from list of selfedges because we already 
                        # used it
                        nws.remove(n) 
        
            # add new paths to list of paths (exclude the old path)
            if len(new_paths) > 1:        
                paths_to_target = paths_to_target + new_paths[1:]
        
    return paths_to_target


def path_to_edgelist(path):
    '''Convert a list of the format [v1,v2,v3] to [(v1,v2),(v2,v3)].'''
    
    edgelist = [(path[i],path[i+1]) for i in range(len(path)-1)]
    return edgelist



def matching_covariance_process_motifs(g):
    '''Find matching covariance process motifs for a graph g.'''
    
    configs = []
    
    # find source and target
    node_labels = nx.get_node_attributes(g, 'label')
    try:
        i = [key  for (key, value) in node_labels.items() if value == 'i'][0]
        j = [key  for (key, value) in node_labels.items() if value == 'j'][0]
    except:
        raise ValueError('g is not graph with 2 focal nodes')

    #get paths
    paths_to_i = [[]] + all_trails_to(g,i) #all_paths_to(g,s)
    #print(paths_to_s)
    paths_to_j = [[]] + all_trails_to(g,j) #all_paths_to(g,t)
    #print(paths_to_t)

    #number of edges
    num_edges = g.number_of_edges()
    edges = sorted(list(g.edges()))

    for pi in paths_to_i:
        #print('pi',pi)
        for pj in paths_to_j:
            #print('pj',pj)
            if len(pi):
                if len(pj):
                    if pi[0][0]==pj[0][0]: 
                        #print('this is a covariance process motif')
                        if len(pi)+len(pj)==num_edges: 
                            # This might be a matching covariance process motif
                            if sorted(pi+pj)==edges:
                                # This is a matching covariance process motif!
                    
                                # add to list
                                configs = configs + [[len(pi)+len(pj),len(pj)]]
                else: # second walk has length zero
                    if pi[0][0]==i:
                        # This is a (one-walk) covariance process motif
                        if len(pi)==num_edges:
                            # This might be a matching (one-walk) covariance 
                            # process motif'
                            if sorted(pi)==edges:
                                #This is a matching (one-walk) covariance
                                # process motif! 
                                # Add process motif to list
                                configs = configs + [[len(pi)+len(pj),len(pj)]]
                                
            else: 
                # First walk has length zero
                if len(pj):
                    if pj[0][0]==j: 
                        # This is a (one-walk) covariance process motif
                        if len(pj)==num_edges:
                            # This might be a (one-walk) matching covariance 
                            # process motif
                            if sorted(pj)==edges:
                                # This is a (one-walk) matching covariance 
                                # process motif!
                                # Add process motif to list
                                configs = configs + [[len(pi)+len(pj),len(pj)]]
                
    return configs


def cov_contribution(motif):
    '''Compute contribution of a covariance process motif.'''
    
    c = binom(motif[0],motif[1])/2**(motif[0]+1)
    
    return c


def approximate_specific_covariance_contribution(G, approximation='all', 
                                                 adjacency_matrix=None, 
                                                 epsilon=0.49):
    '''Compute approximation for specific contribution to covariance 
    from all matching motifs (approximation='all') or the one matching motif
    that contributes the most (approxmation='max').'''
    
    if adjacency_matrix is None:
        adjacency_matrix = nx.to_numpy_matrix(g)
        
    factor = (np.product(adjacency_matrix[adjacency_matrix!=0])
              *epsilon**G.number_of_edges())
    
    contributions = [cov_contribution(m) 
                     for m in matching_covariance_process_motifs(G)]
    if len(contributions)>0:
        if approximation=='all':
            return sum(contributions)*factor
        elif approximation=='max':
            return max(contributions)*factor
        else:
            raise ValueError('Unknown keyword argument for approximation. Must be "all" or "max".')
    else:
        return 0

    
################################################################################
#### FUNCTIONS FOR COMPUTING CONTRIBUTIONS #####################################
################################################################################

def multinomial_coefficient(ints):
    '''Compute multinomial coefficient from a sequence of integers.
    
    Code adapted from:
    https://stackoverflow.com/a/46374719. 
    
    For explanation of multinomial coefficients, see:
    http://mathworld.wolfram.com/MultinomialCoefficient.html
    
    Parameters
    ----------
    ints : list
       A list of integers n1, n2, ...
    
    Returns
    -------
    int m
       the multinomial coefficient of n1, n2, ...
    '''

    try:
        from scipy.special import binom
    except ImportError:
        raise ImportError("scipy required for multinomial_coefficient()")

    if len(ints) <= 1:
        return 1
    
    m = binom(sum(ints), ints[-1]) * multinomial_coefficient(ints[:-1])
    return m
    
    
def integer_compositions(n): 
    '''Construct a generator for integer compositions of n.
    
    Code adapted from:
    http://jeromekelleher.net/generating-integer-partitions.html
    
    For explanation of integer compositions, see "combinatorial composition" at:
    http://mathworld.wolfram.com/Composition.html
    
    This is a helper function for the function `scale_coefficient`.

    Parameters
    ----------
    n : int
       A list of integers n1, n2, ...
    
    Returns
    -------
    generator 
       Generator for all integer compositions of n
    '''
    
    for k in range(1,n):
        for ic in integer_compositions(n-k):
            yield [k]+ic
    if n>0:
        yield [n]
    if n==0:
        yield []
        
        

def scale_coefficient(n,k):
    '''Compute coefficient for mean subsystem contribution in the 
    non-recursive equation for specific contributions.
    
    Parameters
    ----------    
    n : int
       Size of system or number of nodes in network
       
    k : int
       Size of subsystem or number of nodes in subnetwork
       
    Returns
    -------
    float sc
       Scale coefficient for mean subsystem entropy in calculation of specific 
       entropy contributions
    '''
    
    sc = 0
    for c in integer_compositions(n-k):
        sc += (-1)**len(c)*multinomial_coefficient(c)
    sc = sc*binom(n,k)
    return sc


def solveLyapunov(A,Q):
    '''Solve Lyapunov equation A X + X (A^T) + Q = 0. 
    
    There is a solver for this scipy, but it seems buggy somehow?
    
    Parameters
    ----------
    A : 2D numpy array
    
    Q : 2D numpy array
    
    Returns
    -------
    numpy 2D array X
        the solution of the Lyapunv equation
    '''

    try:
        from scipy.linalg import solve_sylvester
    except ImportError:
        raise ImportError("scipy required for solveLyapunov()")

    X = solve_sylvester(A, A.conj().transpose(), Q)
    return X



def covariance(g, epsilon, adjacency_matrix=None):
    '''Compute the covariance between two focal nodes in a graph.
    
    Compute the steady-state covariance matrix of a multivariate Ornstein--
    Uhlenbeck process via solving the Lyapunov equation.
    The result is exact up to numerical error.
    
    Parameters
    ----------
    
    g : a networkX graph
       a networkX graph in which one node has the label "i" and one node has 
       the label "j"
    
    epsilon : float
       Coupling parameter of the Ornstein--Uhlebeck process
       
    adjacency_matrix : numpy matrix (optional)
       If adjacency matrix is given, use that instead of nx.to_numpy_matrix(g)
       to compute covariance. (The argument g is still used to locate the 
       source node and the target node.) This is relevant when wanting to use 
       a different normalization than the implemented one (which is the square 
       root of the Frobenius norm) or when computing 
       mean_subsystem_covariance().
       
    include_offset : bool (optional)
       If true, return entropy value that includes the term that depends only
       on system size. If false, return only the entropy value that depends
       on coupling. Default is False.
    
    Returns
    -------
    float c
       Value of covariance between source and target for a multivariate 
       Ornstein--Uhlenbeck process
    '''

    try:
        from scipy.linalg import logm
    except ImportError:
        raise ImportError("scipy required for covariance()")
        
    # find source and target
    node_labels = nx.get_node_attributes(g, 'label')

    try:
        i = (list(node_labels.values())).index('i')
        j = (list(node_labels.values())).index('j')
    except:
        raise ValueError('g is not a graph with two focal nodes')
    
    # get adjacency matrix
    if adjacency_matrix is None:
        # get adjacency matrix
        A = nx.to_numpy_matrix(g)
        # normalize adjacency matrix
        A = A/np.sqrt(np.sum(A))
    else:
        A = adjacency_matrix
    
    n = len(A)
    #print(A)
    S = solveLyapunov(epsilon*A.transpose()-np.eye(n),-np.eye(n))*2 #THE TRANSPOSE IS IMPORTANT!
    c = S[i,j]
    return c



def correlation(g, epsilon, adjacency_matrix=None):
    '''Compute the correlation between two focal nodes in a graph.
    
    Compute the steady-state covariance matrix of a multivariate Ornstein--
    Uhlenbeck process via solving the Lyapunov equation.
    The result is exact up to numerical error.
    
    Parameters
    ----------
    
    g : a networkX graph
       a networkX graph in which one node has the label "i" and one node has 
       the label "j"
    
    epsilon : float
       Coupling parameter of the Ornstein--Uhlebeck process
       
    adjacency_matrix : numpy matrix (optional)
       If adjacency matrix is given, use that instead of nx.to_numpy_matrix(g)
       to compute covariance. (The argument g is still used to locate the 
       source node and the target node.) This is relevant when wanting to use 
       a different normalization than the implemented one (which is the square 
       root of the Frobenius norm) or when computing 
       mean_subsystem_covariance().
       
    include_offset : bool (optional)
       If true, return entropy value that includes the term that depends only
       on system size. If false, return only the entropy value that depends
       on coupling. Default is False.
    
    Returns
    -------
    float c
       Value of covariance between source and target for a multivariate 
       Ornstein--Uhlenbeck process
    '''
    try:
        from scipy.linalg import logm
    except ImportError:
        raise ImportError("scipy required for correlation()")
        
    # find source and target
    node_labels = nx.get_node_attributes(g, 'label')
    #print(node_labels)
    #print('test')
    #print(list(node_labels.values()))
    try:
        i = (list(node_labels.values())).index('i')
        j = (list(node_labels.values())).index('j')
    except:
        raise ValueError('g is not a source-target graph')
        #return np.nan
    
    # get adjacency matrix
    if adjacency_matrix is None:
        # get adjacency matrix
        A = nx.to_numpy_matrix(g)
        # normalize adjacency matrix
        A = A/np.sqrt(np.sum(A))
    else:
        A = adjacency_matrix
    
    n = len(A)
    #print(A)
    S = solveLyapunov(epsilon*A.transpose()-np.eye(n),-np.eye(n))*2 #THE TRANSPOSE IS IMPORTANT!
    c = S[i,j]/S[i,i]/S[j,j]
    
    return c


def mean_subsystem_covariance(g, k, epsilon, adjacency_matrix=None):
    '''Compute mean subsystem differential entropy for a network structure.
    
    Compute mean subsystem differential entropy for the steady state of a
    multivariate Ornstein--Uhlenbeck process with adjacency matrix A and 
    coupling parameter epsilon. This function samples *all* subsystems, so the
    result has no sampling error and is exact up to some numerical error.
    
    Parameters
    ----------
    g : a networkX graph
       a networkX graph in which one node has the label "i" and one node has 
       the label "j"

    k : int
       Size of subsystem, i.e., the number of EDGES in subnetwork
    
    epsilon : float
       Coupling parameter of the Ornstein--Uhlenbeck process
       
    adjacency_matrix : numpy matrix (optional)
       If adjacency matrix is given, use that instead of nx.to_numpy_matrix(g)
       to compute covariance. (The argument g is still used to locate the 
       source node and the target node.) This is relevant when wanting to use 
       a different normalization than the implemented one (which is the square 
       root of the Frobenius norm) or when computing 
       mean_subsystem_covariance().       
       
    Returns
    -------
    float c
       mean subsystem covariance
    '''

    try:
        import itertools
    except ImportError:
        raise ImportError("itertools required for mean_subsystem_entropy()")

    # find source and target
    node_labels = nx.get_node_attributes(g, 'label')
    try:
        i = (list(node_labels.values())).index('i')
        j = (list(node_labels.values())).index('j')
    except:
        # g is not a source-target graph
        return 0 #return []

    # get adjacency matrix
    if adjacency_matrix is None:
        # get adjacency matrix
        A = nx.to_numpy_matrix(g)
        # normalize adjacency matrix
        A = A/np.sqrt(np.sum(A))
    else:
        A = adjacency_matrix
   
    #loop over all subsystems of size k
    node_list = [x for x in range(len(A)) if g.nodes[x]['label'] not in ['i','j']]
    cs = []
    for p in itertools.combinations(g.edges, len(g.edges)-k):
        # make a subgraph
        g_temp = nx.subgraph_view(g, filter_edge=lambda x,y: False if (x,y) in p else True)
        # make subgraph adjacency matrix
        B = np.copy(A)
        # remove a set of edges
        for e in p:
            B[e[0],e[1]]=0
        # compute contribution to covariance
        c_val = covariance(g_temp, epsilon, adjacency_matrix=B) 
        # add computed value to list of values
        cs = cs + [c_val]

    # compute mean contribution
    c = np.nanmean(cs)
   
    return c


def mean_subsystem_correlation(g, k, epsilon, adjacency_matrix=None):
    '''Compute mean subsystem differential entropy for a network structure.
    
    Compute mean subsystem differential entropy for the steady state of a
    multivariate Ornstein--Uhlenbeck process with adjacency matrix A and 
    coupling parameter epsilon. This function samples *all* subsystems, so the
    result has no sampling error and is exact up to some numerical error.
    
    Parameters
    ----------
    g : a networkX graph
       a networkX graph in which one node has the label "i" and one node has 
       the label "j"

    k : int
       Size of subsystem, i.e.,  number of EDGES in subnetwork
    
    epsilon : float
       Coupling parameter of the Ornstein--Uhlenbeck process
       
    adjacency_matrix : numpy matrix (optional)
       If adjacency matrix is given, use that instead of nx.to_numpy_matrix(g)
       to compute covariance. (The argument g is still used to locate the 
       source node and the target node.) This is relevant when wanting to use 
       a different normalization than the implemented one (which is the square 
       root of the Frobenius norm) or when computing 
       mean_subsystem_covariance().       
       
    Returns
    -------
    float c
       mean subsystem covariance
    '''

    try:
        import itertools
    except ImportError:
        raise ImportError("itertools required for mean_subsystem_entropy()")

    # find source and target
    node_labels = nx.get_node_attributes(g, 'label')
    try:
        i = (list(node_labels.values())).index('i')
        j = (list(node_labels.values())).index('j')
    except:
        # g is not a graph with 2 focal nodes
        return 0

    # get adjacency matrix
    if adjacency_matrix is None:
        # get adjacency matrix
        A = nx.to_numpy_matrix(g)
        # normalize adjacency matrix
        A = A/np.sqrt(np.sum(A))
    else:
        A = adjacency_matrix
    
    #loop over all subsystems of size k
    node_list = [x for x in range(len(A)) if g.nodes[x]['label'] not in ['i','j']]
    cs = []
    for p in itertools.combinations(g.edges, len(g.edges)-k):
        #print(p)
        g_temp = nx.subgraph_view(g, filter_edge=lambda x,y: False if (x,y) in p else True)
        # make a copy of the adjacency matrix
        B = np.copy(A)
        # remove a set of edges (but keep normalization as before)
        for e in p:
            B[e[0],e[1]]=0
        # compute contribution to correlation
        c_val = correlation(g_temp, epsilon, adjacency_matrix=B)
        # add to list of contributions
        cs = cs + [c_val]

    # compute mean contribution
    c = np.nanmean(cs)
   
    return c


def specific_covariance_contribution(g, epsilon, adjacency_matrix=None):
    '''Compute a graphlet's specific contribution to the covariance between
    two nodes.
    
    Compute contribution of a graphlet with adjacency matrix A to the
    the covariance between two nodes for the steady state of a multivariate 
    Ornstein--Uhlenbeck process with coupling parameter epsilon.   
    
    Parameters
    ----------
    g : a networkX graph
       a networkX graph
           
    epsilon : float
       Coupling parameter of the Ornstein--Uhlebeck process

    adjacency_matrix : numpy matrix (optional)
       If adjacency matrix is given, use that instead of nx.to_numpy_matrix(g)
       to compute covariance. (The argument g is still used to locate the 
       source node and the target node.) This is relevant when wanting to use 
       a different normalization than the implemented one (which is the square 
       root of the Frobenius norm) or when computing 
       mean_subsystem_covariance().       
              
    Returns
    -------
    float c
       Specific entropy contribution of graphlet
    '''
    
    # get adjacency matrix
    if adjacency_matrix is None:
        A = nx.to_numpy_matrix(g)
    
        # normalize adjacency matrix
        A = A/np.sqrt(np.sum(A))
    else:
        A = adjacency_matrix
            
    c = 0
    for k in range(1, len(g.edges)+1):
        c += (scale_coefficient(len(g.edges),k)
              *mean_subsystem_covariance(g,k,epsilon,adjacency_matrix=A))

    return c


def specific_correlation_contribution(g, epsilon, adjacency_matrix=None):
    '''Compute a graphlet's specific contribution to the correlation between
    two nodes.
    
    Compute contribution of a graphlet with adjacency matrix A to the
    the covariance between two nodes for the steady state of a multivariate 
    Ornstein--Uhlenbeck process with coupling parameter epsilon.   
    
    Parameters
    ----------
    g : a networkX graph
       a networkX graph
           
    epsilon : float
       Coupling parameter of the Ornstein--Uhlebeck process

    adjacency_matrix : numpy matrix (optional)
       If adjacency matrix is given, use that instead of nx.to_numpy_matrix(g)
       to compute covariance. (The argument g is still used to locate the 
       source node and the target node.) This is relevant when wanting to use 
       a different normalization than the implemented one (which is the square 
       root of the Frobenius norm) or when computing 
       mean_subsystem_covariance().       
              
    Returns
    -------
    float c
       Specific entropy contribution of graphlet
    '''
    
    # get adjacency matrix
    if adjacency_matrix is None:
        A = nx.to_numpy_matrix(g)
    
        # normalize adjacency matrix
        A = A/np.sqrt(np.sum(A))
    else:
        A = adjacency_matrix
            
    c = 0
    for k in range(1, len(g.edges)+1):
        c += (scale_coefficient(len(g.edges),k)
              *mean_subsystem_correlation(g,k,epsilon,adjacency_matrix=A))

    return c


def top_graphlets(q, n, m, epsilon, directed=True, selfedges=True, cutoff=30, 
                  isolates=False, connected=True, flops=False, a_val=1.0,
                  specific=False, load=True, save=True, directory='', 
                  subdirs=[], approximation=None): 
    '''Find graphlets with n nodes and m edges that have the largest (or 
    smallest) contributions to the quantity q.
    
    Parameters
    ----------
    q : ['covariance' | 'cov' | 'correlation' | 'cor' | 'corr' ]
       String indicating the quantity that should be used to rank graphlets. The
       quantity can either be 'covariance' or 'correlation'. Short-cuts 'cov' 
       for 'covariance' and 'cor' or 'corr' for 'correlation' are also accepted.
    
    n : int
       Number of nodes in graphlet
    
    m : int
       Number of edges in graphlet
       
    epsilon : float
       Coupling parameter of the Ornstein--Uhlenbeck process
       
    directed : bool (optional)
       If True, consider directed graphlets. If False, consider undirected 
       graphlets. Default is True.
       
    selfedges : bool (optional)
       If True, consider graphlets with self-edges. If False, consider only
       graphlets without self-edges. Default is True.
       
    isolates : bool (optional)
       If False, exclude all graphlets that include isolated nodes.
       
    connected : bool (optional)
       If True, consider only (weakly) connected graphs. Default is False.

    cutoff : int
       Minimum number of graphlets to be returned. For cutoff=x, the function 
       returns x or more graphlets. It returns more than x graphlets if several
       graphlets share the xth place in the contribution ranking. Default is 20.
       
    flops : bool (optional)
       If True, return graphlets with the smallest contribution instead of 
       graphlets with the largest contribution. Default is False.
        
    specific : bool (optional)
       If True, compute specific contributions to q. Otherwise compute total
       contributions. Default is False.
       
    load : bool (optional)
       If True, try to load graplets and their contributions from file. 
       Default is True.
    
    save : bool (optional)
       If True, save list of graphlets and their contributions to file. 
       Default is True.

    directory : string (optional)
       Path to where the contribution data is saved.
       
    subdirs : list of two strings (optional)
       Relative paths form `directory` to where the approximate contribution 
       data is saved. subdirs[0] is the path to where data for the heuristic 
       that uses all matching process motifs is saved. subdirs[1] is the path 
       to where data for the heuristic that uses the matching process motif 
       with largest process-motif contribution is saved.

    approximation : [ None | 'all' | 'max' ] (optional)
       If approximation is `None` compute contributions; if approximation is 
       'all' compute heuristic using all matching process motifs; if
       approximation is 'max' compute heuristic using the matching process
       motif with largest process-motif contribution
       
    a_val : float (optional)
       Value for non-zero elements of the adjacency matrix of a graph
        
    Returns
    -------
    list graphs_and_qs
       List in which each element is [g, c] with a graphlet g and its 
       contribution to the quantity q.
    '''

    try:
        import os
    except ImportError:
        raise ImportError("os required for top_graphlets()")

    # construct filename for load and save options
    s = ('flop' if flops else 'top')
    s += ('S' if specific else 'T')
    
    #if q in ['entropy', 'ent']:
    #    s += 'E'     
    if q in ['covariance', 'cov']:
        s += 'C'
    elif q in ['correlation', 'corr', 'cor']:
        s += 'R'
        
    else:
        raise Exception('Unknown quantity argument:', q)
        return np.nan #[]
    
    filename = filename_for_graphs(s+'Cs_'+str(epsilon)+'_', n, m, directed, 
                                   selfedges)         
    filename += '_cutoff' + str(cutoff) + '.p'
    
    if approximation is None:
        new_directory = directory
    elif approximation=='all':
        new_directory = directory + subdirs[0]
    elif approximation=='max':
        new_directory = directory + subdirs[1]
    else:
        raise ValueError("Unknown input for keyword argument `approximation`. Must be None, 'all', or 'max'.")

    # try to load results from file
    if load and os.path.exists(new_directory+filename):
        # read results from file
        #print('read graphs and qs from file')
        graphs_and_qs = read_pickle(new_directory+filename)
        return graphs_and_qs

    # otherwise initialize lists
    graphs_and_qs = []
    qs = []

    # construct q function
    if q in ['covariance', 'cov']:
        if specific:
            if approximation is None:
                compute_q = lambda g, A: specific_covariance_contribution(g, epsilon,adjacency_matrix=A)
            else:
                compute_q = lambda g, A: approximate_specific_covariance_contribution(g, epsilon=epsilon,
                                                                                      adjacency_matrix=A,
                                                                                      approximation=approximation)
        else:
            compute_q = lambda g, A: covariance(g, epsilon,adjacency_matrix=A)
    else: 
        # q is not 'cov' or 'covariance' ... must be 'correlation'
        if specific:
            compute_q = lambda g, A: specific_correlation_contribution(g, epsilon,adjacency_matrix=A)
        else:
            compute_q = lambda g, A: correlation(g, epsilon,adjacency_matrix=A)
        
    
    # set the graphs that we want to loop over
    if q in ['entropy','ent']:
        # to be implemented later
        graphs = nonisomorphic_graphs(n, m, directed=directed, 
                                      selfedges=selfedges, load=True,
                                      isolates=isolates,
                                      directory=directory) 
    else:
        # q is covariance or correlation. we need a graph with focal nodes
        graphs = nonisomorphic_2FN_graphs(n, m, directed=directed, 
                                          selfedges=selfedges, load=True,
                                          isolates=isolates,
                                          directory=directory) 
        
    # loop over all non-isomorphic graphs with n nodes and m edges
    for g in graphs:
        
        if connected:
            # skip disconnected graphs
            if directed:
                if not nx.is_weakly_connected(g):
                    continue
            else:
                if not nx.is_connected(g):
                    continue

        # compute q    
        A = nx.to_numpy_matrix(g)*a_val
        q = compute_q(g, A)
        
        # add results to lists
        qs = qs + [q]
        graphs_and_qs = graphs_and_qs + [[g, q]]

    # if m is too large, there are no graphs with n nodes and m edges
    # and no elements have been added to matrices_and_secs
    if len(graphs_and_qs)==0:
        return []

    # sort results by q so that largest qs are first
    secs = sorted(qs, key=lambda x: -x)
    graphs_and_qs = sorted(graphs_and_qs, key=lambda x: -x[-1])   
    
    # if interested in lowest qs, reverse order so that lowest qs are first
    if flops:
        secs = secs[::-1]
        graphs_and_qs = graphs_and_qs[::-1]       
    
    # If list is longer than cutoff, we want to shorten it
    if len(qs) > cutoff: 
        # We want to include only graphlets with q>=t
        t = qs[cutoff]
        # find last occurence of t in secs (which is a sorted list)
        real_cutoff = len(qs) - 1 - qs[::-1].index(t) 
        # remove all graphlets after last occurrence of t
        graphs_and_qs = graphs_and_qs[:real_cutoff]
        
    if save:
        # save results to file
        write_pickle(new_directory+filename, graphs_and_qs)

    return graphs_and_qs


def all_top_graphlets(q, m, epsilon, directed=True, selfedges=True, cutoff=30, 
                      isolates=False, connected=True, specific=False, a_val=1.0,
                      flops=False, load=True, save=True, directory='', 
                      subdirs=['all/','max/'], approximation=None): 
    '''Find graphlets with m edges that have the largest (or smallest) 
    contributions to the quantity q. q can be 'covariance' or 'correlation'.
    
    Parameters
    ----------
    q : ['covariance' | 'cov' | 'correlation' | 'cor' | 'corr' ]
       String indicating the quantity that should be used to rank graphlets. The
       quantity can either be 'covariance' or 'correlation'. Short-cuts 'cov' 
       for 'covariance' and 'cor' or 'corr' for 'correlation' are also accepted.
        
    m : int
       Number of edges in graphlet
       
    epsilon : float
       Coupling parameter of the Ornstein--Uhlenbeck process
       
    directed : bool (optional)
       If True, consider directed graphlets. If False, consider undirected 
       graphlets. Default is True.
       
    selfedges : bool (optional)
       If True, consider graphlets with self-edges. If False, consider only
       graphlets without self-edges. Default is True.
       
    isolates : bool (optional)
       If False, exclude all graphlets that include isolated nodes.
       
    connected : bool (optional)
       If True, consider only (weakly) connected graphs. Default is True for 
       when q=='covariance' and False if q=='entropy'.
       
    cutoff : int (optional)
       Minimum number of graphlets to be returned. For cutoff=x, the function 
       returns x or more graphlets. It returns more than x graphlets if several
       graphlets share the xth place in the ranking. Default is 20.
       
    flops : bool (optional)
       If True, return graphlets with the smallest contribution instead of 
       graphlets with the largest contribution. Default is False.
        
    specific : bool (optional)
       If True, compute specific contributions to q. Otherwise compute total
       contributions. Default is False.
       
    load : bool (optional)
       If True, try to load graplets and their contributions from file. 
       Default is True.
  
    save : bool (optional)
       If True, save list of graphlets and their contributions to file. 
       Default is True.

    directory : string (optional)
       Path to where the contribution data is saved.
       
    subdirs : list of two strings (optional)
       Relative paths form `directory` to where the approximate contribution 
       data is saved. subdirs[0] is the path to where data for the heuristic 
       that uses all matching process motifs is saved. subdirs[1] is the path 
       to where data for the heuristic that uses the matching process motif 
       with largest process-motif contribution is saved.

    approximation : [ None | 'all' | 'max' ] (optional)
       If approximation is `None` compute contributions; if approximation is 
       'all' compute heuristic using all matching process motifs; if
       approximation is 'max' compute heuristic using the matching process
       motif with largest process-motif contribution
       
    a_val : float (optional)
       Value for non-zero elements of the adjacency matrix of a graph
        
    Returns
    -------
    list graphs_and_qs
       List in which each element is a [g, c] with a graphlet g and its 
       contribution to the quantity q.
    '''
    
    tgs = []
    for n in range(1,m+2):        
        tgs = tgs + top_graphlets(q, n, m, epsilon, directed=directed, 
                                  selfedges=selfedges, cutoff=cutoff, 
                                  isolates=isolates, connected=connected,
                                  flops=flops, specific=specific, a_val=a_val,
                                  load=load, save=save, directory=directory,
                                  subdirs=subdirs,
                                  approximation=approximation)
        
    # sort results by q so that largest qs are first
    tgs = sorted(tgs, key=lambda x: -x[-1])
    qs = [x[1] for x in tgs]
    
    # if interested in lowest qs, reverse order so that lowest qs are first
    if flops:
        qs = qs[::-1]
        tgs = tgs[::-1]
    
    # if list is longer than cutoff, we want to shorten it
    if len(qs) > cutoff:
        # We want to include only graphlets with q>=t        
        t = qs[cutoff]
        # find last occurence of t in qs (which is a sorted list)
        real_cutoff = len(qs) - 1 - qs[::-1].index(t) 
        # remove all graphlets after last occurrence of t
        tgs = tgs[:real_cutoff]       
        
    return tgs
    
    
################################################################################

def initial_computation(m_range=range(1,6), 
                        flops=[False, True], 
                        directed=[True, False], 
                        selfedges=[True, False], 
                        specific=[True, False],
                        epsilon=[0.1, 0.49], 
                        cutoff=30,
                        directory='',
                        subdirs=['all/', 'max/'], 
                        approximation=None,
                        a_val = 1/np.sqrt(6),
                        load=True, 
                        verbose=True):
    '''Wrapper for running initial computation to get data for plotting
    structure motifs and their contributions. Data is saved automatically. This
    function has no return value.
    
    Parameters
    ----------
    m_range : Integer or a list of integers (optional)
       These are the values of the number m of edges for which we compute 
       contributions.
       
    flops : Boolean or list of Booleans (optional)
       If flops is `False` find structure motifs with largest contributions;
       if flops is `True` find structure motifs with smallest contributions;
       if flops is `[True, False]` consider both cases.
       
    directed : Boolean or list of Booleans (optional)
       If directed is `True` compute contributions for directed structure motifs;
       if directed is `False` compute contributions for undirected structure 
       motifs; if directed is `[True, False]` consider both cases.
       
    selfedges : Boolean or list of Booleans (optional)
       If selfedges is `True` compute contributions for structure motifs with
       self-edges; if selfedges is `False` compute contributions for structure 
       motifs without self-edges; if selfedges is `[True, False]` consider both 
       cases.
       
    specific : Boolean or list of Booleans (optional)
       If specific is `True` compute specific contributions; if specific is 
       `False` compute total contributions; if specific is `[True, False]` 
       consider both cases.
       
    epsilon : Float of list of floats (optional)
       Value or values of the coupling parameter for the Ornstein--Uhlenbeck 
       process
       
    cutoff : Integer (optional)
       Maximal number of structure motifs and structure motif contributions to
       be saved. This is important because one probably does not want to clogg 
       up space with thousands of structure motifs. In practice, the number of
       saved structure motifs and structure motif contributions can be larger
       than cutoff when structure motifs tie for contributions.
       
    directory : string (optional)
       Path to where the contribution data is saved.
       
    subdirs : list of two strings (optional)
       Relative paths form `directory` to where the approximate contribution 
       data is saved. subdirs[0] is the path to where data for the heuristic 
       that uses all matching process motifs is saved. subdirs[1] is the path 
       to where data for the heuristic that uses the matching process motif 
       with largest process-motif contribution is saved.

    approximation : [ None | 'all' | 'max' ] (optional)
       If approximation is `None` compute contributions; if approximation is 
       'all' compute heuristic using all matching process motifs; if
       approximation is 'max' compute heuristic using the matching process
       motif with largest process-motif contribution.
       
    a_val : float (optional)
       Value for non-zero elements of the adjacency matrix of a graph.

    verbose : Boolean (optional)
       If verbose is `True` print updates on the status of the computation.
'''
    
    # check validity of inputs
    if not hasattr(m_range, '__iter__'):
        if isinstance(m_range, int):
            m_range = [m_range]
        else:
            raise ValueError('keyword argument m_range must be integer or list of integers.')

    if not hasattr(epsilon, '__iter__'):
        if isinstance(epsilon, float):
            epsilon = [epsilon]
        else:
            raise ValueError('keyword argument epsilon must be float or list of floats.')
            
    if not hasattr(flops, '__iter__'):
        if isinstance(flops, bool):
            flops = [flops]
        else:
            raise ValueError('keyword argument flops must be Boolean or list of Booleans.')
            
    if not hasattr(directed, '__iter__'):
        if isinstance(directed, bool):
            directed = [directed]
        else:
            raise ValueError('keyword argument directed must be Boolean or list of Booleans.')

    if not hasattr(selfedges, '__iter__'):
        if isinstance(selfedges, bool):
            selfedges = [selfedges]
        else:
            raise ValueError('keyword argument selfedges must be Boolean or list of Booleans.')
            
    if not hasattr(specific, '__iter__'):
        if isinstance(specific, bool):
            specific = [specific]
        else:
            raise ValueError('keyword argument specific must be Boolean or list of Booleans.')
            
    # start the loop over all keyword arguments
    for m in m_range:
        for f in flops: 
            for d in directed: 
                for se in selfedges: 
                    for sp in specific:
                        for eps in epsilon: 
                            t0=time.time()
                            #print('start', m, f, d, se, sp, eps)

                            # start computation
                            graphlets = all_top_graphlets('cov', m, eps, 
                                                          directed=d, 
                                                          selfedges=se, 
                                                          specific=sp, 
                                                          flops=f, 
                                                          directory=directory, 
                                                          subdirs=subdirs,
                                                          approximation=approximation,
                                                          load=load,
                                                          save=True,
                                                          cutoff=cutoff,
                                                          a_val=a_val)
                            
                            graphlets = all_top_graphlets('cor', m, eps, 
                                                          directed=d, 
                                                          selfedges=se, 
                                                          specific=sp, 
                                                          flops=f, 
                                                          directory=directory, 
                                                          subdirs=subdirs,
                                                          approximation=approximation,
                                                          load=load,
                                                          save=True,
                                                          cutoff=cutoff,
                                                          a_val=a_val)

                            if verbose:
                                # make a status update on completing on case
                                print(m, f, d, se, sp, eps, len(graphlets), 
                                      time.time()-t0)
    print('Initial computation complete!')
    
    
################################################################################
#### FUNCTIONS FOR DRAWING GRAPHS ##############################################
################################################################################

def _compute_node_extent(node_size, figure=None, ax=None):
    '''Compute the radius of a node in units of an axis' x and y axis. This is 
    a helper functions for `draw_networkx_selfedges`.
    
    Parameters
    ----------
    node_size : float
       The size of a node in the same units as used in nx.draw() or
       nx.draw_networkx_nodes().
       
    fig : figure (optional)
       Default is pyplot.gcf().
       
    ax : axis (optional)
       Default is pyplot.gca().

    Returns
    -------
    tuple (dx,dy)
        dx is radius in units of x axis, dy is radius in units of y axis.
    '''

    try:
        from matplotlib import pyplot as plt
    except ImportError:
        raise ImportError("Matplotlib required for _compute_node_extent()")

    try:
        import numpy as np
    except ImportError:
        raise ImportError("numpy required for _compute_node_extent()")

    if figure is None:
        figure = plt.gcf()
    if ax is None:
        ax = plt.gca()
    xlims = ax.get_xlim()
    ylims = ax.get_ylim()
    ax_extent_x = xlims[1]-xlims[0] # extent of x axis
    ax_extent_y = ylims[1]-ylims[0] # extent of y axis 
    ax_width_inch = (ax.get_position()).width*figure.get_size_inches()[0]
    ax_height_inch = (ax.get_position()).height*figure.get_size_inches()[1]

    node_radius_pts = np.sqrt(node_size)/2.0 # node radius in points
    node_radius_inch = node_radius_pts/72.0 # node radius in inch
    node_radius_extent_x = node_radius_inch/ax_width_inch*ax_extent_x
    node_radius_extent_y = node_radius_inch/ax_height_inch*ax_extent_y

    return(node_radius_extent_x, node_radius_extent_y)


def draw_networkx_selfedges(G, pos, node_size=300, shift=1, arc_size=0.5, 
                            orientation='radial', selfedge_color=None, 
                            **kwds):
    '''Draw self-edges for graph G.

    Draws hollow circles behind nodes to visualize self-edges. Parameters 
    set location and size of circle.

    Parameters
    ----------
    G : graph
       A networkx graph

    pos : dictionary
       A dictionary with nodes as keys and positions as values.
       Positions should be sequences of length 2.

    node_size : scalar or array, optional
       Size or sizes of nodes in the G's visualization. The value of  
       node_size should be the same when calling nx.draw(), 
       nw.draw_networkx(), or nx.draw_networkx_nodes() before or after 
       calling draw_networkx_selfedges(). Default is 300.

    shift : float (optional)
       Sets distance between the center point of a node and the center
       point of its self-edge. Default is 1.

    arc_size : float (optional)
       Sets ratio between radius of node and radius of self-edge arcs.
       Default is 0.5.

    orientation : [float | iterable (dictionary, list, array) | "radial"] (optional)
       Defines the direction in which self-edge arcs are shifted. Numerical
       values are interpreted as angles in degrees.  A value of 0 degrees
       indicates an orientation parallel to the x-axis. If value is 
       "radial", all self-edge arcs are shifted away from the axis' origin.

    selfedge_color : color string, or array of floats
       Color of self-edges. Default is 'k' or the value of the keyword 
       argument edge_colors if passed.

    linewidths : float or sequence (optional)
       Line width for self-edge arcs. Default is 1.0.

    Returns
    -------
    matplotlib.collections.PathCollection
        `PathCollection` of the self-edges.

    Examples
    --------
    >>> g = nx.dodecahedral_graph()
    >>> g.add_edges_from([[i,i] for i in range(11)])
    >>> pos = nx.layout.shell_layout(g)
    >>> nx.draw(g,pos)
    >>> draw_networkx_selfedges(g,pos)
    '''

    try:
        import numpy as np
    except ImportError:
        raise ImportError("numpy required for draw_networkx_selfedges()")

    # check if pos is an admissable dictionary of node positions
    keys = pos.keys()
    num_pos = len(keys)
    num_nodes = G.number_of_nodes()
    if num_pos < num_nodes:
        raise Exception('Number of entries in position dictionary needs to be equal to or greater than the number of nodes in G.')

    # radii of nodes in data coordinates
    if isinstance(node_size, (int, float)):
        radius = _compute_node_extent(node_size)
        radii = radius*np.ones((num_nodes,2))
    elif hasattr(node_size, '__iter__'):
        if len(node_size)==num_nodes:
            radii = np.array([compute_node_extent(node_size[i]) for i 
                              in range(num_nodes)])
        else:
            raise Exception('node_size must be number or array with length equal to number of nodes in G.')
    else:
        raise Exception('node_size must be number or array.')

    # angles of selfedges
    nodes = G.nodes()
    if isinstance(orientation, (int, float)):
        # convert degrees to radiants
        alpha = orientation*np.pi/180.0*np.ones(num_nodes)
    elif isinstance(orientation, dict):
        try:
            alpha = np.pi/180.0*np.array([orientation[i] for i in nodes])
        except KeyError:
            print('Keys in dictionary alpha do not match nodes in G.')
    elif hasattr(orientation, '__iter__'):
        if isinstance(orientation,str):
            if orientation == "radial":
                # compute angles from node positions
                alpha = np.array([np.arctan2(pos[i][1], pos[i][0]) for i 
                                 in pos.keys() if i in nodes]) 
            else:
                raise Exception('Unknown string argument for orientation.')
        elif len(orientation)==num_nodes:
            # convert degrees to radiants
            alpha = np.array(orientation)*np.pi/180.0
        else:
            raise Exception('If orientation is non-string iterable, its length must be equal to number of nodes.')
    else:
        raise Exception('orientation must be number, iterable, or "radial".')

    # compute shifts for each possible selfedge position
    dx = shift*np.cos(alpha)*radii[:,0]
    dy = shift*np.sin(alpha)*radii[:,1]
    pos2 = {k: [pos[k][0]+dx[i], pos[k][1]+dy[i]] for i,k 
            in enumerate(pos.keys()) if k in nodes}
    
    # set color of selfedges
    if selfedge_color is None:
        if 'edge_color' in kwds.keys():
            selfedge_color = kwds['edge_color']
        else:
            selfedge_color = 'k'

    # subgraph with nodes that have selfedges
    edges = G.edges()
    G2 = nx.Graph()
    for n in nodes:
        if (n,n) in edges:
            G2.add_node(n)
            
    # if there are no selfeges return empty list
    if not G2.number_of_nodes():
        return []
    
    # draw selfedges
    edge_collection = nx.draw_networkx_nodes(G2, pos2, 
                                             node_size=node_size*arc_size,
                                             edgecolors=selfedge_color, 
                                             node_color='none', **kwds)
    edge_collection.set_zorder(0)
    
    return edge_collection


def _layout_cost(G, positions):
    '''Computes total length of edges in a graph's visualization.
    
    This is a helper function for `optimal_permutation_layout`.
    
    Parameters
    ----------
    G : graph
       A networkx graph.
       
    positions : dictionary
       A dictionary with nodes as keys and positions as values.
       Positions should be sequences of length 2.
       
    Returns
    -------
    float c
       Total edge length in a visualization of a graph.
    '''

    try:
        import numpy as np
        from numpy.linalg import norm as vector_norm
    except ImportError:
        raise ImportError("numpy required for _layout_cost()")

    n = G.number_of_nodes()
    edges = G.edges()
    out = np.sum([vector_norm(positions[i]-positions[j]) for i in range(n) 
                  for j in range(i) if ((i,j) in edges or (j,i) in edges)])

    return out


def optimal_permutation_layout(G, positions):
    '''Find optimal permutaion of nodes for a given layout.
    
    For a set of given node positions, find permutation of nodes that 
    minimizes the total length of edges in a graph's visualization.
    
    Using this for networks with more than 10ish nodes is a bad idea!
    
    This is a helper functions for the function `oriented_shell`.
    
    Parameters
    ----------
    G : graph
       A networkx graph.
       
    positions : dictionary
       A dictionary with nodes as keys and positions as values.
       Positions should be sequences of length 2.
       
    Returns
    -------
    tuple (new_pos, c)
        new_pos is dictionary of node positions, c is the associated
        total length of edges in visualization.
        
    Examples
    --------
    >>> g = nx.Graph()
    >>> g.add_edges_from([[0,3],[1,3],[1,4],[2,3],[3,5]])
    >>> pos = nx.layout.shell_layout(g)
    >>> pos2, c = optimal_permutation_layout(g, pos)
    >>> nx.draw(g, pos2)
    '''
    
    try:
        import itertools
    except ImportError:
        raise ImportError("itertools required for optimal_permutation_layout()")
    
    edges = G.edges()
    node_labels = positions.keys()
    lowest_cost = 1E10
    for p in itertools.permutations(node_labels):
        current_cost = _layout_cost(G, {i:positions[p[i]] for i in node_labels})
        if lowest_cost > current_cost:
            lowest_cost = current_cost
            lowest_cost_p = p
    # new positions
    pos_new = {i:positions[lowest_cost_p[i]] for i in node_labels}

    return pos_new, lowest_cost


def optimal_permutation_layout_with_constraints(G, positions, fixed_nodes):
    '''Find optimal permutaion of nodes for a given layout with 
    constraints.
    
    For a set of given node positions, find allowed permutation of nodes
    that minimizes the total length of edges in a graph's visualization. 
    One can force a subset of nodes to keep their position.
    
    Using this for networks with more than 10ish nodes is a bad idea!
    
    This is a helper functions for the function `oriented_shell`.
    
    Parameters
    ----------
    G : graph
       A networkx graph.
       
    positions : dictionary
       A dictionary with nodes as keys and positions as values.
       Positions should be sequences of length 2.
       
    fixed_nodes : list or array
       List of node labels of nodes that should keep their position.
       If fixed_nodes is [], this function outputs the same as 
       optimal_permutation_layout() but can take longer to compute.
       
    Returns
    -------
    dict new_pos
        dictionary of node positions
        
    float c
        c is the associated total length of edges in the visualization
        
    Examples
    --------
    >>> g = nx.Graph()
    >>> g.add_edges_from([[0,3],[1,3],[1,4],[2,3],[3,5]])
    >>> pos = nx.layout.shell_layout(g)
    >>> pos2, c = optimal_permutation_layout(g, pos, [1,2])
    >>> nx.draw(g, pos2)
    '''
    try:
        import itertools
    except ImportError:
        raise ImportError("itertools required for optimal_permutation_layout_with_constraints()")
        
    edges = G.edges()
    node_labels = positions.keys()
    flexible_nodes = [x for x in node_labels if x not in fixed_nodes]
    permuted_positions = dict(positions)
    lowest_cost = 1E10
    for p in itertools.permutations(flexible_nodes):
        for i, n in enumerate(flexible_nodes):
            permuted_positions[n] = positions[p[i]]
        current_cost = _layout_cost(G, permuted_positions)
        if lowest_cost > current_cost:
            lowest_cost = current_cost
            lowest_cost_p = p
    # new positions
    for i, n in enumerate(flexible_nodes):
        permuted_positions[n] = positions[lowest_cost_p[i]]

    return permuted_positions, lowest_cost


def oriented_shell(G, select="degree"): 
    '''Find optimal shell layout with a select node on the right.
    
    Find the shell layout for a graph G that minimizes the total length
    of edges when a node with maximum degree or a node with a selfedge is
    in the far right position.
    
    Using this for networks with more than 10ish nodes is a bad idea!
    
    This is a helper function for the function `plot_structure_motifs`.
    
    Parameters
    ----------
    G : graph
       A networkx graph.
       
    select : ["degree" | "self-edge"], optional
       When select is "degree" (or "d"), the node on the far right is a 
       node with maximum degree. When select is "self-edge" (or "se"), 
       the node on the far right is a node with a self-edge. Default is
       "degree".
              
    Returns
    -------
    dict new_pos
        new_pos is dictionary of node positions
        
    float lowest_cost
        Total edge length associated with the final layout
        
    Examples
    --------
    >>> g = nx.Graph()
    >>> g.add_edges_from([[0,3],[1,3],[1,4],[2,3],[3,5],[1,1],[2,2]])
    >>> pos = oriented_shell(g)
    >>> nx.draw(g, pos)
    '''

    try:
        import numpy as np
        from numpy.linalg import norm as vector_norm
    except ImportError:
        raise ImportError("numpy required for oriented_shell()")

    # position for node with selfedge
    position0 = np.array([1.0,0.0])

    # generate positions for nodes in shell layout
    shell = nx.layout.shell_layout(G)

    # set criterion for special nodes
    edges = G.edges()
    if select in ["degree", "deg", "d"]:
        max_deg = np.max(np.array(G.degree())[:,1])
        special_nodes =  [n for n in G.nodes() if G.degree(n)==max_deg]
    elif select in ["self-edge", "self_edge", "selfedge", "se"]:
        # mark nodes with selfedges as special nodes
        special_nodes = [n for n in G.nodes() if (n,n) in edges]
    else:
        raise Exception('Unknown value for keyword argument select. Choose "degree" (or "d") or "self-edge" (or "se").')
    if len(special_nodes)==0:
        # there is nothing to orient here
        # return an optimal shell layout without constraints
        return optimal_permutation_layout(G, shell)[0]

    # find the node that currently has the position that a sepcial node 
    # should have
    for k in shell.keys():
        # allow for small numeric error
        if vector_norm(shell[k]-position0) < 1E-6:
            node0, position0 = k, shell[k]
            break 

    # try different node switches
    lowest_cost = 1E10
    for node1 in special_nodes:
        #hard copy position dictionary
        shell2 = dict(shell)
        # switch node positions so that node1 is on the right        
        shell2[node0] = shell[node1]
        shell2[node1] = position0
        # get optimal shell with node1 on the right
        os, c = optimal_permutation_layout_with_constraints(G, shell2, 
                                                            [node1])
        if lowest_cost > c:
            lowest_cost = c
            lowest_cost_shell = os

    return lowest_cost_shell, lowest_cost


def plot_structure_motifs(q, m, epsilon, 
                          directed=True, selfedges=True, 
                          specific=True, flops=False, 
                          cutoff=30, a_val = 1/np.sqrt(6),
                          load=True, save=True,
                          directory='data/', approximation=None,
                          subdirs=['all/', 'max/'],
                          precision=4):
    '''Plot structure motifs and their contributions to covariance or correlation.
    
    Parameters
    ----------
    q : ['covariance' | 'cov' | 'correlation' | 'cor' | 'corr' ]
       String indicating the quantity that should be used to rank graphlets. The
       quantity can either be 'covariance' or 'correlation'. Short-cuts 'cov' 
       for 'covariance' and 'cor' or 'corr' for 'correlation' are also accepted.
        
    m : int
       Number of edges in graphlet
       
    epsilon : float
       Coupling parameter of the Ornstein--Uhlenbeck process
       
    directed : bool (optional)
       If True, consider directed graphlets. If False, consider undirected 
       graphlets. Default is True.
       
    selfedges : bool (optional)
       If True, consider graphlets with self-edges. If False, consider only
       graphlets without self-edges. Default is True.
       
    connected : bool (optional, to be implemented)
       If True, consider only (weakly) connected graphs. Default is True for 
       when q=='covariance' and False if q=='entropy'.
              
    flops : bool (optional)
       If True, return graphlets with the smallest contribution instead of 
       graphlets with the largest contribution. Default is False.
        
    specific : bool (optional)
       If True, compute specific contributions to q. Otherwise compute total
       contributions. Default is False.

    cutoff : int (optional)
       Minimum number of graphlets to be returned. For cutoff=x, the function 
       returns x or more graphlets. It returns more than x graphlets if several
       graphlets share the xth place in the ranking. Default is 20.

    a_val : float (optional)
       Value for non-zero elements of the adjacency matrix of a graph
       
    load : bool (optional)
       If True, try to load graplets and their contributionss from file. 
       Default is True.
    
    save : bool (optional)
       If True, save list of graphlets and their contributions to file. 
       Default is True.

    directory : string (optional)
       Path to where the contribution data is saved.
       
    approximation : [ None | 'all' | 'max' ] (optional)
       If approximation is `None` compute contributions; if approximation is 
       'all' compute heuristic using all matching process motifs; if
       approximation is 'max' compute heuristic using the matching process
       motif with largest process-motif contribution

    subdirs : list of two strings (optional)
       Relative paths form `directory` to where the approximate contribution 
       data is saved. subdirs[0] is the path to where data for the heuristic 
       that uses all matching process motifs is saved. subdirs[1] is the path 
       to where data for the heuristic that uses the matching process motif 
       with largest process-motif contribution is saved.
    
    precision : int (optional)
       Number of decimal points in display of contribution values.
'''    
    
    graphlets = all_top_graphlets(q, m, epsilon, specific=specific, flops=flops,
                                  directed=directed, selfedges=selfedges,
                                  cutoff=cutoff, a_val=a_val, 
                                  load=load, save=save, 
                                  directory=directory, 
                                  approximation=approximation,
                                  subdirs=subdirs)
    # set notation for contribution
    notation = 'c'
    if specific:
        notation = '\hat ' + notation
    if approximation=='all':
        notation = '\gamma_{'+ notation + '}'
    elif approximation=='max':
        notation = '\gamma_{'+ notation + '}'
    
    plt.figure(figsize=(8,2*np.ceil(len(graphlets)/4.0)))
    
    for i, p in enumerate(graphlets):
        
        g = p[0] 
        shell, c = oriented_shell(g) 
        ax=plt.subplot(int(np.ceil(len(graphlets)/4.0)),4,i+1)
        
        #ax.set_xlim([-1.65,1.65])
        #ax.set_ylim([-1.65,1.65])
        #nx.draw(g, shell)
        #draw_networkx_selfedges(g, shell)
        
        if len(graphlets) > i :
            #print(graphlets[i])
            g = graphlets[i][0]
                                       
            shell, x = oriented_shell(g)
            if g.number_of_nodes()==4:
                shell[0]=np.array([1.0/np.sqrt(2),1.0/np.sqrt(2)])
                shell[1]=np.array([-1.0/np.sqrt(2),1.0/np.sqrt(2)])
                shell[2]=np.array([1.0/np.sqrt(2),-1.0/np.sqrt(2)])
                shell[3]=np.array([-1.0/np.sqrt(2),-1.0/np.sqrt(2)])
                        
                    
            k = 1 # k > 1 make everything smaller
            if g.number_of_nodes()==3:
                ax.set_xlim(np.array([-1.75,1.75])/k+0.1/k)
                ax.set_ylim(np.array([-1.8,1.7])/k+0.15/k)
                        
                # permute shell if necessary to move self-edges to bottom
                        
                # 1. check if at least one node does not have a self-edge
                if not([0,0] in g.edges() and [1,1] in g.edges()
                       and [2,2] in g.edges()):

                    # 2. find node at the top
                    top_node = np.argmax([shell[index][1] for index in range(3)])
                            
                    # 3. permute until good
                    while [top_node,top_node] in g.edges():
                        shell = {0:shell[1], 1:shell[2], 2:shell[0]}
                        # 3a. find node at the top again
                        top_node = np.argmax([shell[index][1] for index in range(3)])

            else:
                ax.set_xlim(np.array([-1.75,1.75])/k)
                ax.set_ylim(np.array([-1.75,1.75])/k+0.25/k)
                       
            # make shell smaller
            for j in shell:
                shell[j] = shell[j]/k        
        
        # draw graph
        nx.draw(g, shell, node_size=300, node_color='white', edgecolors='k', linewidths=1)
        draw_networkx_selfedges(g, shell)
        
        # node labels
        focal_nodes = []
        labels = nx.get_node_attributes(g,'label')
        for k in labels:
            if labels[k]=='i':
                labels[k]=r'$i$'
                focal_nodes = focal_nodes + [k]
            elif labels[k]=='j':
                labels[k]=r'$j$'
                focal_nodes = focal_nodes + [k]
        nx.draw_networkx_labels(g, shell, labels, font_color='black', font_size=12)
        
        ax.set_facecolor('#ffedd1')
        ax.axis('on')

        ax.text(0.975,0.94, #1,1.1,
                ('$'+notation+'='+('\,$' if graphlets[i][1]>=0 else '-$')
                 +"{:.{prec}f}".format(np.abs(np.real(p[1])), prec=precision)), 
                transform=ax.transAxes,
                horizontalalignment='right', verticalalignment='top')