# Supplementary Material for "Motifs for processes on networks"

Use the Jupyter notebook in this repository to view structure motifs and their contributions to covariance and correlation in the multivariate Ornstein--Uhlenbeck process. The notebook is live on Binder. Click the "Launch Binder" icon to open the interactive notebook in your browser.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/aliceschwarze%2Fmotifs-for-processes/master)
